function dashboard(){
       $('#div_dashboard').show();
       $('#div_information').hide();
       $('#div_address').hide();
       $('#div_orders').hide();
       $('#div_creditcard').hide();
       $('#div_wishlist').hide();
   }
   function information(){
       //alert("hello");
       $('#div_dashboard').hide();
       $('#div_information').show();
       $('#div_address').hide();
       $('#div_orders').hide();
       $('#div_creditcard').hide();
       $('#div_wishlist').hide();
   }
   function address(){
       $('#div_dashboard').hide();
       $('#div_information').hide();
       $('#div_address').show();
       $('#div_orders').hide();
       $('#div_creditcard').hide();
       $('#div_wishlist').hide();
   }
   function orders(){
       $('#div_dashboard').hide();
       $('#div_information').hide();
       $('#div_address').hide();
       $('#div_orders').show();
       $('#div_creditcard').hide();
       $('#div_wishlist').hide();
   }
   function creditcard(){
       $('#div_dashboard').hide();
       $('#div_information').hide();
       $('#div_address').hide();
       $('#div_orders').hide();
       $('#div_creditcard').show();
       $('#div_wishlist').hide();
   }
   function wishlist(){
       $('#div_dashboard').hide();
       $('#div_information').hide();
       $('#div_address').hide();
       $('#div_orders').hide();
       $('#div_creditcard').hide();
       $('#div_wishlist').show();
   }