from shop.models import Product


class MiddlewareProduct(object):
    """Middleware class to get all the products from database
    """
    def process_request(self, request):
        products = Product.objects.all()
        #define the total_page which is used later in loadmoreitems.
        total_page = len(products)//3
        #make the list only with starting 3product items.
        #append products and total_page to request.
        request.products = products[:3]
        request.page = total_page
        return None