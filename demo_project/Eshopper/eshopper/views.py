__author__ = 'mindfire solutions'
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from decimal import Decimal
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import patterns, url
from django.shortcuts import render
from shop.util.decorators import on_method, shop_login_required, order_required
from shop.models import Order, OrderItem, Cart, Product
from django.views.generic import View
import logging
import json

logger = logging.getLogger(__name__)


def checkout(request):
    """This functions checks whether the user is logged in or not. For checkout, the user has to log in.
    """
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('checkout_selection'))
    else:
        return HttpResponseRedirect('/accounts/login')


class BaseShipping(object):
    """
    This is a simple free shipping backend that sets the shipping cost to 0
    and makes the shipping component of checkout invisible to the user
    """
    url_namespace = 'shipping'
    backend_name = 'Base shipping'

    def __init__(self, shop):
        self.shop = shop
        self.rate = 0
        self.rates_dict = {}

    @on_method(shop_login_required)
    @on_method(order_required)
    def select_method(self, request):
        """This method is used to get the order details
        """
        try:
            carts = Cart.objects.filter(user_id=request.user.id)[0]

            if carts:
                cart = carts
            else:
                return render(request, "shop/checkout/order_details.html", {'none': True})

            order = Order.objects.filter(cart_pk=cart.id).order_by('-created')[0]
            address_ship = order.shipping_address_text
            address_bill = order.billing_address_text
            list_ship = [x.strip() for x in address_ship.split(',')]
            list_bill = [x.strip() for x in address_bill.split(',')]
            order_items = OrderItem.objects.filter(order_id=order.id)
            orders = order_items
            return render(request, "shop/checkout/order_details.html", {'order_items': orders,
                                                                        'order': order,
                                                                        'list_b': list_bill,
                                                                        'list_s': list_ship})

        except Exception as e:
            logger.error(e)
            return render(request, "shop/checkout/order_details.html", {'none': True})

    @on_method(shop_login_required)
    @on_method(order_required)
    def view_process_order(self, request):
        """A simple (not class-based) view to process an order.

        This will be called by the selection view (from the template) to do the
        actual processing of the order (the previous view displayed a summary).

        It calls shop.finished() to go to the next step in the checkout
        process.
        """
        service_method = request.POST.get('method')
        self.rate = self.rates_dict.get(service_method, 0)
        request.session['shipping_charge'] = self.rate
        self.shop.add_shipping_costs(self.shop.get_order(request),
                                     service_method,
                                     Decimal(self.rate))
        return self.shop.finished(self.shop.get_order(request))

    def get_urls(self):
        """Return the list of URLs defined here.
        """
        urlpatterns = patterns('',
                               url(r'^$', self.select_method, name="shipping"),
                               url(r'^complete/$', self.view_process_order, name="shipping_method"),
                               )
        return urlpatterns

@csrf_exempt
def product_search():
    """
    This method returns list of search results.
    :param request:
    :return: search_products.
    """
    #this is called when the person enters the key in search box.
    # on ech press of key this part will be called to get a list of search suggetions.
    if request.method == 'POST':
        search = []
        search_key = request.POST['key']
        # if the entered key is not null get the list of search results.
        if search_key != '':
            search_products = Product.objects.filter(slug__contains=search_key)
        # appends the name of the search products to the list.
        for products in search_products:
            search.append(products.name)
        data = json.dumps({"key": search})
        return HttpResponse(data, json)
    else:
        # when the user submit a search query this part will be executed.
        if request.method == 'GET':
            search_key = request.GET['search'].lower()
            # get the list of results from table Product.
            search_products = Product.objects.filter(slug__contains=search_key)
            return render(request, "search_results.html", {'search_products': search_products,
                                                           'search_key': request.GET['search']})


class LoadMoreItems(View):

    @csrf_exempt
    def post(self, request):
        """This method is used to get more items on click of load_more button

        :param request:
        :return:
        """
        product_list = []
        #if request.method == 'POST':
        key = request.POST['key']
        start = 3 * int(key)
        end = 3 + int(start)
        product = Product.objects.all()
        #if list of products is not null.
        if product:
            products = product[start:end]

        #creates a dictionary for each item in Products.
        for item in products:
            product_dict = {}
            product_dict['category'] = item.category
            product_dict['name'] = item.name
            product_dict['image'] = item.image.name
            product_dict['unit_price'] = float(item.unit_price)
            product_dict['slug'] = item.slug
            product_dict['id'] = item.id
            product_list.append(product_dict)

        # Jason object for list for products.
        data = json.dumps({'key': product_list})
        return HttpResponse(data, json)

