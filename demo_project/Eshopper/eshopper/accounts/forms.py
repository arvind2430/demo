from django import forms


class AuthenticationForm(forms.Form):
    """Form for user signin.
    """
    username = forms.CharField(required=True, widget=forms.TextInput(attrs={'type': 'text'}))
    password = forms.CharField(required=True, max_length = 20, widget=forms.TextInput(attrs={'type':'password' }))