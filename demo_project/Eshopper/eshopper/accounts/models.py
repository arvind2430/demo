from django.db import models
from django.contrib.auth.models import User


class Address(models.Model):
    """Extended model for Address to store shipping_address and billing_address.
    """
    user_shipping = models.OneToOneField(User, related_name='shipping_address', blank=True, null=True)
    user_billing = models.OneToOneField(User, related_name='billing_address', blank=True, null=True)
