from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import make_password, User
from eshopper.accounts.forms import AuthenticationForm
from shop.models import Order
from wishlist.models import WishlistItem
import logging
logger = logging.getLogger(__name__)


def account_select(request):
    """This is the login function. Checks the credentials.
    """
    # Post method for login
    if request.method == 'POST':
        auth_form = AuthenticationForm(request.POST)

        # If the login form is valid
        if auth_form.is_valid():

            username = auth_form.cleaned_data['username']
            password = auth_form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            # If the user is created after valid information
            if user:
                login(request, user)
                request.session['USER_ID'] = user.pk
                return HttpResponseRedirect(reverse('checkout'))

            # Wrong set of credentials provided
            else:
                messages.error(request, "Wrong username and Password combination.")
                return HttpResponseRedirect(reverse('accounts_login'))

        # If the login form is invalid
        else:
            messages.error(request, "The fields cannot be left empty.")
            return HttpResponseRedirect(reverse('accounts_login'))

    # Get method for login
    elif request.method == 'GET':
        auth_form = AuthenticationForm()

        # If the user is already logged in
        if request.session.get('USER_ID', ''):
            use_next = reverse('checkout')

            # Redirects the logged in user to the checkout page
            if use_next:
                return HttpResponseRedirect(use_next)

        return render(request, "login.html", {
            'auth_form': auth_form,
        })

    # Any hardcode way method injection by user
    else:
        messages.error(request, "The loading of the form was not proper. Please try again.")
        return HttpResponseRedirect('accounts_login')


def account_signup(request):
    """This is the signup function. Asks for username & password & confirm password.
    """
    # Post method for Signup
    if request.method == 'POST':
        user_form = UserCreationForm(request.POST)

        # Signup form is valid
        if user_form.is_valid():
            data = user_form.data
            udata = {}

            # Validation of matching of both the passwords
            if data['password1'] == data['password2']:
                try:
                    udata['username'] = data['username']
                    udata['password'] = make_password(data['password1'], salt="acc")
                    user = User.objects.create(**udata)

                    # New user created after validation
                    if user:
                        user = authenticate(username=udata['username'], password=data['password1'])
                        login(request, user)
                        request.session['USER_ID'] = user.pk
                        return HttpResponseRedirect('/')

                    else:
                        messages.error(request, "Provide a different username.")
                        return HttpResponseRedirect(reverse('signup'))

                # Exception handled while validation
                except Exception as e:
                    logger.error(e)
                    messages.error(request, "Provide the correct information in all the fields.")
                    return HttpResponseRedirect(reverse('accounts_signup'))

            else:
                messages.error(request, "Provide the same passwords in both the fields.")
                return HttpResponseRedirect(reverse('signup'))

        # If the Signup form is invalid
        else:
            messages.error(request, "Please provide valid information.")
            return HttpResponseRedirect(reverse('signup'))

    # Get method for Signup
    elif request.method == 'GET':
        user_form = UserCreationForm()

        if request.session.get('USER_ID', ''):
            use_next = reverse('accounts_login')

            if use_next:
                return HttpResponseRedirect(use_next)

        return render(request, "signup.html", {
            'user_form': user_form,
        })

    # Any hardcode way method injection by user
    else:
        messages.error(request, "The loading of the form was not proper. Please try again.")
        return HttpResponseRedirect('signup')


def show_form(request):
    """Shows the login/signup form if the user is not logged in.
    """
    if request.method == 'GET':
        user_form = UserCreationForm()
        auth_form = AuthenticationForm()

        # Checking the session for logged in user
        if request.session.get('USER_ID', ''):
            use_next = reverse('accounts_login')

            if use_next:
                return HttpResponseRedirect(use_next)

        return render(request, "signup.html", {
            'user_form': user_form,
            'auth_form': auth_form,
        })

    else:
        return HttpResponseRedirect()


def logout_view(request):
    """This is the logout function.
    """
    # Logging out the logged in user
    logout(request)
    HttpResponseRedirect('accounts_login')
    # Redirect to a success page.


def account(request):
    """This method is used to get all the wishlist items of user
    """
    # Filters the orders & wishlist for the logged in user
    orders = Order.objects.filter(user_id=request.user.id)
    wishlists = WishlistItem.objects.filter(user_id=request.user.id)
    return render(request, "accounts.html", {'orders': orders, 'wishlists': wishlists})
