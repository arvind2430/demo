from __future__ import print_function
from cms.sitemaps import CMSSitemap
from django.conf.urls import *
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from shop import urls as shop_urls
from eshopper.views import LoadMoreItems

admin.autodiscover()

urlpatterns = i18n_patterns('',
    url(r'^accounts/', include('eshopper.accounts.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^checkout/', 'eshopper.views.checkout', name='checkout'),
    url(r'^search', 'eshopper.views.product_search', name='search'),
    url(r'^load_more', LoadMoreItems.as_view(), name='load_more'),
    url(r'^users/', include('allauth.urls')),
    url(r'^shop/', include(shop_urls)),
    url(r'^wishlist/', include('wishlist.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': {'cmspages': CMSSitemap}}),
    url(r'^', include('cms.urls'), ),
)