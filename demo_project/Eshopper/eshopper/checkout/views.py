from django.shortcuts import render


def checkout(request):
    """ Render the template for checkout"""
    return render(request, "shop/checkout/selection.html")
